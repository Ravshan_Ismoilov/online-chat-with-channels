from django.db import models
from autoslug import AutoSlugField
from django.contrib.auth import get_user_model

User = get_user_model()


class Room(models.Model):
	name = models.CharField(max_length=255, unique=True, verbose_name="Чат")
	slug = AutoSlugField(populate_from='name', unique=True, null=False, editable=False)
	creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name="chat_creator", verbose_name="Создатель")
	created = models.DateTimeField(auto_now_add=True, verbose_name="Дата добавления")

	def __str__(self):
		return f'{self.name}'


class Message(models.Model):
	room = models.ForeignKey(Room, on_delete=models.CASCADE, related_name="message", verbose_name="Сообщение")
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user", verbose_name="Отправитель")
	text = models.TextField()
	created = models.DateTimeField(auto_now_add=True, verbose_name="Дата добавления")

	class Meta:
		ordering = ('created',)