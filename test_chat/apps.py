from django.apps import AppConfig


class TestChatConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'test_chat'
