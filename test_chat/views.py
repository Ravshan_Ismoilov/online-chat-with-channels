from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from test_chat.models import *
from test_chat.forms import *

@login_required
def rooms(request):
	rooms = Room.objects.all()
	form = AddChatsForms()
	return render(request, 'rooms.html', locals())

@login_required
def chat(request, slug=None):

	if slug:
		room = get_object_or_404(Room, slug=slug)
	else:
		if request.method == 'POST':
			form = AddChatsForms(request.POST)
			if form.is_valid():
				cd = form.cleaned_data
				name = cd['name']
				user = request.user
				room = Room.objects.create(name=name, creator=user)
			else:
				messages.error(request, "This chat already exists.")
				return redirect('chat:rooms')
		else:
			return redirect('chat:rooms')
	all_messages = Message.objects.filter(room=room).all()
	count = room.message.values('user').distinct().count()
	return render(request, 'chat.html', locals())