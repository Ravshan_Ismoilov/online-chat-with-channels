from django.contrib import admin
from test_chat.models import *


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
	list_display = ('name', 'slug', 'creator', 'created')
	list_filter = ('created',)
	readonly_fields = ('slug', 'creator', 'created')
	search_fields = ('name', 'creator', 'created')
	save_on_top = True

	def save_model(self, request, obj, form, change):
		obj.creator = request.user
		super().save_model(request, obj, form, change)

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
	list_display = ('room', 'user', 'text', 'created')
	list_filter = ('room', 'created')
	readonly_fields = ('created',)
	search_fields = ('room__name', 'creator__username', 'created')
	save_on_top = True

	def save_model(self, request, obj, form, change):
		obj.creator = request.user
		super().save_model(request, obj, form, change)
