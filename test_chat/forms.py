from django import forms
from test_chat.models import *

class AddChatsForms(forms.ModelForm):
    class Meta:
        model = Room
        fields = ('name',)