from django import template
from test_chat.models import Message

register = template.Library()

@register.simple_tag
def room_date(pk, room):
    messages = Message.objects.filter(room=room).all()
    all_true = False
    for message in messages:
        if message.pk == pk:
            all_true = True
            sana = '<p class="date">{}</p>'.format(message.created.strftime("%d.%m.%Y"))
        elif all_true:
            sana2 = '<p class="date">{}</p>'.format(message.created.strftime("%d.%m.%Y"))
            if sana2 != sana:
                return f'{sana2}'
            break
        if pk == 1:
            return sana
    return ''

@register.simple_tag
def user_msg(pk, room, user):
    try:
        pk = int(pk)
    except Exception as e:
        print(e)
        print(pk)
        print(type(pk))

    if Message.objects.filter(room=room, user=user, pk=pk-1).exists():
        return ''
    return 'index'